FROM maven:3.8.1-jdk-11-slim
RUN apt-get update && \
    apt-get install -y --no-install-recommends xvfb openjfx libgtk-3-0 libglu1-mesa && \
    rm -rf /var/lib/apt/lists/*