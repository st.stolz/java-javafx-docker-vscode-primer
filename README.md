# java javafx docker vscode primer

## Usage

1. Clone Repo and open root folder in vscode
2. Install vscode "Remote - Containers" Extension
3. Select "View" - "Command Palette ..." in vscode and search "Remote Containers: Open Folder in Container ..."
4. Open "_20210417_javafx_simple.application/application/App.java" and run it
5. Go to http://ip-to-system-running-docker:8080/vnc.html in your browser
