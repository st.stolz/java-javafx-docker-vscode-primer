# JavaFX simple Example

* Man unterteilt ein JavaFX Programm logisch in [Stage, Scene, Node](https://panjutorials.de/tutorials/javafx-8-gui/lektionen/unser-erstes-fenster-stage-scene-und-node-in-javafx/)
* Man ordnet Nodes mit den [verschiedenen Layout Panes](https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm) an. Diese sind selbst Nodes.
* Mit fxml kann man das Layout optimal von der Logik trennen. Hier gibt es [ein gutes Tutorial](https://javabeginners.de/Frameworks/JavaFX/FXML.php) und ein noch [ausführlicheres Tutorial](http://tutorials.jenkov.com/javafx/fxml.html).

## Tutorials 

* [Oracle Tutorial](https://docs.oracle.com/javafx/2/get_started/jfxpub-get_started.htm)

## Projektstruktur

https://stackoverflow.com/questions/31135192/fxmlloader-constructloadexception-when-trying-to-run-java-fx-application

```
src
|-main
    |-java <-- This should be your 'sources root'
        |-application <-- The application itself is within the new sources root
        |-controllers
            |here are my controllers
        |-dao
        |-service
    |-resources <-- all resources (css files, images, fxml files) should be with a subfolder of the resources. This folder should be marked as 'resources root'
        |-css
        |-images
        |-view
            |here are the fxml files
```