package _20210417_javafx_simple.controllers;

import _20210417_javafx_simple.domain.Node;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class BlockchainController {

    @FXML
    private FlowPane blockFlow;

    public static Node node = new Node();

    private int blockCounter = 2;

    @FXML
    protected void mine(ActionEvent event) {

        if (node.getMemPool().size() > 0) {
            node.newBlock();

            System.out.println(node.toString());
            Image image = new Image("/images/block.png");
            ImageView imgView = new ImageView();
            imgView.setImage(image);
            imgView.setFitWidth(150);
            imgView.setPreserveRatio(true);
            Label infoBlock = new Label("Block " + blockCounter);

            StackPane blockView;
            blockView = new StackPane();

            VBox vbox = new VBox();
            vbox.setPadding(new Insets(75, 0, 0, 56));
            vbox.getChildren().add(infoBlock);

            blockView.getChildren().add(imgView);
            blockView.getChildren().add(vbox);

            blockFlow.getChildren().add(blockView);
            blockCounter++;


        } else {
            Alert emptyMempool = new Alert(Alert.AlertType.ERROR);
            emptyMempool.setTitle("Error");
            emptyMempool.setHeaderText("Mempool is empty");
            emptyMempool.show();
        }
    }

    @FXML
    public void newTransaction(ActionEvent actionEvent) {
        TransactionController t = new TransactionController();
        t.newTransaction(node);
    }

    @FXML
    public void newSmartContract(ActionEvent actionEvent) {
        SmartContractController smartContractController = new SmartContractController();
        smartContractController.send(actionEvent);
    }
}