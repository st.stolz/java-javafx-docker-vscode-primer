package _20210417_javafx_simple.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
 
public class FXMLExampleController {

    @FXML private FlowPane blockFlow;
    
    @FXML protected void handleSubmitButtonAction(ActionEvent event) {
        Image image = new Image("/_20210417_javafx_simple/resources/images/block.png");
        ImageView imgView = new ImageView();
        imgView.setImage(image);
        imgView.setFitWidth(150);
        imgView.setPreserveRatio(true);
        blockFlow.getChildren().add(imgView);
        System.out.println("Block added");
    }

}