package _20210417_javafx_simple.controllers;

import _20210417_javafx_simple.application.App;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class SmartContractController {

    @FXML
    private Button newSmartContract = new Button("send");

    @FXML
    private TextField sender, code, signature;

    @FXML
    private TextField senderInput, codeInput, signatureInput;

    @FXML
    protected void send(ActionEvent actionEvent) {
        newSmartContract.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                BlockchainController.node.newSmartContract(senderInput.getText(), codeInput.getText(), signatureInput.getText());
                BlockchainController.node.showMempool();
                App.showInfoDialog("Smart Contract successfully saved", BlockchainController.node.getMemPool().get(BlockchainController.node.getMemPool().size() - 1).toString());
            }
        });
    }
}
