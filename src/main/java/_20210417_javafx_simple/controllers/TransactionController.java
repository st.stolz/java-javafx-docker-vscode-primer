package _20210417_javafx_simple.controllers;

import _20210417_javafx_simple.domain.Node;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TransactionController {

    private Stage transactionStage;
    private Scene transactionScene;
    private GridPane gridPane;
    private TextField sender;
    private TextField receiver;
    private TextField amount;
    private TextField signature;
    private Button send;


    public TransactionController(){
        gridPane = new GridPane();
        transactionScene = new Scene(gridPane, 400,375);
        transactionStage = new Stage();


        transactionStage.setScene(transactionScene);
    }


    public void setGridPane(){
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Transaction");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        gridPane.add(scenetitle, 0, 0, 2, 1);

        Label senderLabel = new Label("Sender");
        Label receiverLabel = new Label ("Receiver");
        Label amountLabel = new Label ("Amount");
        Label signatureLabel = new Label("Signature");


        sender = new TextField();
        receiver = new TextField();
        amount = new TextField();
        signature = new TextField();

        send = new Button("Send");

        gridPane.add(senderLabel, 0, 1);
        gridPane.add(signatureLabel, 0, 4);
        gridPane.add(amountLabel, 0, 3);
        gridPane.add(receiverLabel, 0, 2);

        gridPane.add(sender, 1, 1);
        gridPane.add(receiver, 1, 2);
        gridPane.add(amount, 1, 3);
        gridPane.add(signature, 1, 4);

        gridPane.add(send, 1, 5);


    }


    public void newTransaction(Node _node){
    setGridPane();
    final Node node = _node;
    send.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                node.newTransaction(sender.getText(), receiver.getText(), Double.parseDouble(amount.getText()), signature.getText());
                node.showMempool();
                Alert sucessfullySaved = new Alert(Alert.AlertType.INFORMATION);
                sucessfullySaved.setTitle("Information");
                sucessfullySaved.setHeaderText("Transaction successfully saved");
                sucessfullySaved.setContentText(node.getMemPool().get(node.getMemPool().size() - 1).toString());
                sucessfullySaved.show();
                transactionStage.close();
            }
        });




    }





}
