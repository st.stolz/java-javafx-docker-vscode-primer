package _20210417_javafx_simple.application;


import javafx.application.Application;
import javafx.scene.control.Alert;

public class App
{
    public static void main( String[] args )
    {
        Application.launch(FXMLExample.class, args);
    }

    public static void showInfoDialog(String headertext, String contextText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(headertext);
        alert.setContentText(contextText);
        alert.show();
    }

    public static void showErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("An exception occurred: " + message);
        alert.showAndWait();
    }
}
