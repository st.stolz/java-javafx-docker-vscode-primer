package _20210417_javafx_simple.domain;

public class Contract {
    private String sender;
    private String signature;

    public Contract(String sender, String signature) {
        this.sender = sender;
        this.signature = signature;
    }

    public String getSender() {

        return sender;
    }

    public void setSender(String sender) {

        this.sender = sender;
    }


    public String getSignature() {
        return signature;
    }

    @Override
    public String toString() {
        return "\nsender:" + sender +
                "\nsignature: " + signature;
    }
}
