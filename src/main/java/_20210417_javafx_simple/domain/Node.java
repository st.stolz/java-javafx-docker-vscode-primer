package _20210417_javafx_simple.domain;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Node {
    private ArrayList<Block> blockChain;
    private ArrayList<Contract> memPool;
    private Block genesisBlock;
    static final String DIFFICULTY_FIXED = Integer.toString(ThreadLocalRandom.current().nextInt(0, 10001));

    public Node() {
        this.blockChain = new ArrayList<>();
        this.memPool = new ArrayList<>();
        this.genesisBlock = new Block("000000000000000");
        genesisBlock.mine();
        blockChain.add(genesisBlock);
    }

    public ArrayList<Contract> getMemPool() {
        return memPool;
    }

    public ArrayList<Block> getBlockChain() {
        return blockChain;
    }

    public void getBlockInfo() {

    }

    public void newBlock() {

        String previousHash = blockChain.get(blockChain.size() - 1).getHash();
        Block newBlock = new Block(previousHash);

        for (Contract c : memPool) {
            newBlock.addTransaction(c);
        }

        newBlock.mine();
        blockChain.add(newBlock);

        memPool.clear();
    }

    public void newTransaction(String sender, String receiver, double amount, String signature) {
        try {
            Transaction t = new Transaction(sender, signature, receiver, amount);
            memPool.add(t);
        } catch (NumberFormatException | NullPointerException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void newSmartContract(String sender, String code, String signature) {
        SmartContract s = new SmartContract(sender, signature, code);
        memPool.add(s);
    }


    @Override
    public String toString() {
        String output = "";
        int count = 1;

        for (Block block : blockChain) {
            output += "# Block " + count + block.toString() + "\n";
            count++;
        }
        return output;
    }

    public void showMempool() {

        for (Contract c : memPool) {
            System.out.println("\n" + c.toString());
        }

    }

}
