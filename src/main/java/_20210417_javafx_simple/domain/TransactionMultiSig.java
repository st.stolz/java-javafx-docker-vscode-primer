package _20210417_javafx_simple.domain;

import java.util.ArrayList;

public class TransactionMultiSig extends Transaction {
    ArrayList<String> signatures;


    public TransactionMultiSig(String sender, String signature, String receiver, double amount) {
        super(sender, signature, receiver, amount);
        signatures = new ArrayList<>();
    }
}
