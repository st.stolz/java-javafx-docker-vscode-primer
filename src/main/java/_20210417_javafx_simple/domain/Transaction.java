package _20210417_javafx_simple.domain;

public class Transaction extends Contract {

    String receiver;
    double amount;

    public Transaction(String sender, String signature, String receiver, double amount) {
        super(sender, signature);
        this.receiver = receiver;
        this.amount = amount;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "\nTransaction:" +
                "\nSender: " + getSender() +
                "\nReceiver: " + receiver +
                "\nAmount: " + amount +
                "\nSignature: " + getSignature();
    }
}
