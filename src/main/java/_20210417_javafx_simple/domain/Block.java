package _20210417_javafx_simple.domain;

import java.time.LocalDate;
import java.util.ArrayList;

public class Block {
    private String previousHash;
    private String hash;
    private LocalDate timestamp;
    private ArrayList<Contract> transactions;
    private long nonce;

    public Block(String previousHash) {
        this.previousHash = previousHash;
        this.timestamp = LocalDate.now();
        this.transactions = new ArrayList<>();
    }


    public String getHash() {

        return this.hash;
    }

    public String getTimestamp() {

        return timestamp.toString();
    }

    public void addTransaction(Contract transaction) {

        transactions.add(transaction);
    }

    public void mine () {
        Miner miner = new Miner(generateStrToHash());
        this.hash = miner.mine(Node.DIFFICULTY_FIXED);
        this.nonce = miner.getNonce();
    }

    //Stringbuffer Klasse (append Methode) https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/StringBuffer.html
    public String generateStrToHash() {
        StringBuffer stringToHash = new StringBuffer();

        for (Contract t : transactions) {
            stringToHash.append(t.getSender() + t.getSignature());

            if (t instanceof Transaction){
                stringToHash.append(((Transaction)t).getReceiver() + (((Transaction) t).getAmount()));
            }
        }
        return stringToHash.toString();
    }


    @Override
    public String toString() {
        return "\nPrevious Hash: " + previousHash +
                "\nHash: " + hash +
                "\nTimestamp: " + timestamp +
                "\nNonce: " + nonce +
                "\nDifficulty: " + Node.DIFFICULTY_FIXED +
                "\nNumber of transactions: " + transactions.size();
    }
}
