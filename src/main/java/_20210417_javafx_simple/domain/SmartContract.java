package _20210417_javafx_simple.domain;

public class SmartContract extends Contract {
    private String code;

    public SmartContract(String sender, String signature, String code) {
        super(sender, signature);
        this.code = code;
    }


    @Override
    public String toString() {
        return "\nSmart Contract:" +
                "\nSender: " + getSender() +
                "\nCode: " + code +
                "\nSignature: " + getSignature();
    }
}
